import './App.css';
import { Container, Typography } from '@mui/material';
import Order from './components/Orders';

function App() {
  return (
    <Container maxWidth="md">
      <Typography 
        variant='h2'
        align='center'
        marginBottom={2}>
          Restoraunt App
      </Typography>
      <Order/>
    </Container>
  );
}

export default App;
