import React from 'react';
import TextField from '@mui/material/TextField';
import FormHelperText from '@mui/material/FormHelperText';

export default function Input(props) {
 const {
    name,
    label,
    value,
    variant,
    onChange,
    error = null,
    ...other
 } = props;

 return (
    <React.Fragment>
      <TextField
        fullWidth
        variant={variant || 'outlined'}
        label={label}
        name={name}
        value={value}
        onChange={onChange}
        {...other}
      />
      {error && <FormHelperText error>{error}</FormHelperText>}
    </React.Fragment>
 );
}