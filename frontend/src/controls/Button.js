
import { Button as MuiButton, styled } from '@mui/material';

const Button = styled(MuiButton)(({ theme }) => ({
    '&.orderButton': {
        backgroundColor: '#4caf50',
        color: '#fff',
        border: '1px solid #888', // Set the border color to black
        '&:hover': {
            backgroundColor: '#45a049',
        },
    },
    '&.resetButton': {
        backgroundColor: '#f3b33d',
        color: '#000',
        border: '1px solid #888', // Set the border color to black
        '&:hover': {
            backgroundColor: '#f3b33d',
        },
    },
}));

export default Button;
