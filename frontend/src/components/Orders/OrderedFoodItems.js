import {
  Button,
  ButtonGroup,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  ListItemText,
} from "@mui/material";
import DeleteTwoToneIcon from "@mui/icons-material/DeleteTwoTone";
import { roundTo2DecimalPoint } from "../../utils";

export default function OrderedFoodItems(props) {
  const { values, setValues } = props;

  let orderedFoodItems = values.orderDetails;

  const removeFoodItem = (index, id) => {
    let x = { ...values };
    x.orderDetails = [
      ...x.orderDetails.slice(0, index),
      ...x.orderDetails.slice(index + 1),
    ];
    if (id !== 0) x.deletedOrderItemIds += id + ",";
    setValues({ ...x });
  };

  const updateQuantity = (idx, value) => {
    let x = { ...values };
    let foodItem = x.orderDetails[idx];
    if (foodItem.quantity + value > 0) {
      foodItem.quantity += value;
      setValues({ ...x });
    }
  };

  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Food Item</TableCell>
            <TableCell>Quantity</TableCell>
            <TableCell>Price</TableCell>
            <TableCell>Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {orderedFoodItems.length === 0 ? (
            <TableRow>
              <TableCell colSpan={4} align="center">
                <ListItemText primary="No food items selected" />
              </TableCell>
            </TableRow>
          ) : (
            orderedFoodItems.map((item, idx) => (
              <TableRow key={idx}>
                <TableCell>{item.foodItemName}</TableCell>
                <TableCell>
                  <ButtonGroup size="small">
                    <Button onClick={(e) => updateQuantity(idx, -1)}>-</Button>
                    <Button disabled>{item.quantity}</Button>
                    <Button onClick={(e) => updateQuantity(idx, 1)}>+</Button>
                  </ButtonGroup>
                </TableCell>
                <TableCell>
                  ${roundTo2DecimalPoint(item.quantity * item.foodItemPrice)}
                </TableCell>
                <TableCell>
                  <IconButton
                    disableRipple
                    onClick={(e) => removeFoodItem(idx, item.orderDetailsId)}
                  >
                    <DeleteTwoToneIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
