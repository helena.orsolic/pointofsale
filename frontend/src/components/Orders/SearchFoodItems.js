import React, { useState, useEffect } from "react";
import { createAPIEndpoint, ENDPOINTS } from "../../api";
import { styled } from "@mui/system";
import {
  IconButton,
  InputBase,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Paper,
} from "@mui/material";
import SearchTwoToneIcon from "@mui/icons-material/SearchTwoTone";
import PlusOneIcon from "@mui/icons-material/PlusOne";
import ArrowForwardIos from "@mui/icons-material/ArrowForwardIos";

const SearchPaper = styled(Paper)({
  padding: "2px 4px",
  display: "flex",
  alignItems: "center",
});

const SearchInput = styled(InputBase)(({ theme }) => ({
  marginLeft: theme.spacing(1.5),
  flex: 1,
}));

const ListRoot = styled(List)(({ theme }) => ({
  marginTop: theme.spacing(1),
  maxHeight: 350,
  overflow: "auto",
  "& li:hover": {
    cursor: "pointer",
    backgroundColor: "#E3E3E3",
  },
  "& li:hover .MuiButtonBase-root": {
    display: "block",
    color: "#000",
  },
  "& .MuiButtonBase-root": {
    display: "none",
  },
  "& .MuiButtonBase-root:hover": {
    backgroundColor: "transparent",
  },
}));

export default function SearchFoodItems(props) {
  const { values, setValues } = props;

  let orderedFoodItems = values.orderDetails;

  const [foodItems, setFoodItems] = useState([]);
  const [searchList, setSearchList] = useState([]);
  const [searchKey, setSearchKey] = useState("");

  useEffect(() => {
    createAPIEndpoint(ENDPOINTS.FOODITEM)
      .fetchAll()
      .then((res) => {
        setFoodItems(res.data);
        setSearchList(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    let x = [...foodItems];
    x = x.filter((y) => {
      return y.foodItemName
        .toLowerCase()
        .includes(searchKey.toLocaleLowerCase())
        && orderedFoodItems.every(item => item.foodItemId !== y.foodItemId)
    });
    setSearchList(x);
  }, [searchKey, foodItems, orderedFoodItems]);

  const addFoodItem = (foodItem) => {
    let x = {
      invoiceId: values.invoiceId,
      orderDetailId: 0,
      foodItemId: foodItem.foodItemId,
      quantity: 1,
      foodItemPrice: foodItem.price,
      foodItemName: foodItem.foodItemName,
    };
    setValues({
      ...values,
      orderDetails: [...values.orderDetails, x],
    });
  };

  return (
    <>
      <SearchPaper>
        <SearchInput
          value={searchKey}
          onChange={(e) => setSearchKey(e.target.value)}
          placeholder="Search food items"
        />
        <IconButton>
          <SearchTwoToneIcon />
        </IconButton>
      </SearchPaper>
      <ListRoot>
        {searchList.map((item, idx) => (
          <ListItem key={idx} onClick={() => addFoodItem(item)}>
            <ListItemText
              primary={item.foodItemName}
              secondary={"$" + item.price}
            />
            <ListItemSecondaryAction>
              <IconButton onClick={() => addFoodItem(item)}>
                <PlusOneIcon />
                <ArrowForwardIos />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </ListRoot>
    </>
  );
}
