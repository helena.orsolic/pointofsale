import React, { useEffect, useState } from "react";
import { createAPIEndpoint, ENDPOINTS } from "../../api";
import Table from "../../layouts/Table";
import { TableCell, TableHead, TableRow, TableBody } from "@mui/material";
import DeleteOutlineTwoToneIcon from "@mui/icons-material/DeleteOutlineTwoTone";

export default function OrderList(props) {
  const [orderList, setOrderList] = useState([]);
  const { setOrderId, setOrderListVisibility, resetFormControls } = props;

  useEffect(() => {
    createAPIEndpoint(ENDPOINTS.ORDER)
      .fetchAll()
      .then((res) => {
        setOrderList(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const deleteOrder = (id) => {
    if (window.confirm("Do you want to delete order?")) {
      createAPIEndpoint(ENDPOINTS.ORDER)
        .delete(id)
        .then((res) => {
          setOrderListVisibility(false);
          setOrderId(0);
          resetFormControls()
        })
        .catch((err) => console.log(err));
    }
  };

  return (
    <>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Order No.</TableCell>
            <TableCell>Customer</TableCell>
            <TableCell>Payed With</TableCell>
            <TableCell>Grand Total</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {orderList.map((item) => (
            <TableRow key={item.invoiceId}>
              <TableCell>{item.orderNumber}</TableCell>
              <TableCell>{item.customer.customerName}</TableCell>
              <TableCell>{item.paymentMethod}</TableCell>
              <TableCell>{"$" + item.total}</TableCell>
              <TableCell>
                <DeleteOutlineTwoToneIcon color="secondary" 
                onClick={e => deleteOrder(item.invoiceId)}/>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </>
  );
}
