import React, { useState, useEffect } from "react";
import Form from "../../layouts/Form";
import {
  Grid,
  InputAdornment,
  Typography,
  ButtonGroup,
  styled,
} from "@mui/material";
import { Input, Select, Button } from "../../controls";
import Popup from "../../layouts/Popup";
import ReplayIcon from "@mui/icons-material/Replay";
import RestaurantMenuIcon from "@mui/icons-material/RestaurantMenu";
import ReorderIcon from "@mui/icons-material/Reorder";
import { createAPIEndpoint, ENDPOINTS } from "../../api";
import { roundTo2DecimalPoint } from "../../utils";
import OrderList from "./OrderList";

const pMethods = [
  { id: "none", title: "Select" },
  { id: "Cash", title: "Cash" },
  { id: "Card", title: "Card" },
];

const AdornmentText = styled(Typography)(({ theme }) => ({
  color: "#f3b33d",
  fontWeight: "bolder",
  fontSize: "1.5em",
}));

const SubmitButtonGroup = styled(ButtonGroup)(({ theme }) => ({
  backgroundColor: "#f3b33d",
  color: "#000", // Set text color to black
  marginTop: "1rem", // Set top margin to 1rem or your preferred value
  "& .MuiButton-label": {
    textTransform: "none",
    color: "#000", // Set text color to black
  },
  "&:hover": {
    backgroundColor: "#f3b33d",
  },
}));

export default function OrderForm(props) {
  const {
    values,
    setValues,
    errors,
    setErrors,
    handleInputChange,
    resetFormControls,
  } = props;

  const [customerList, setCustomerList] = useState([]);
  const [orderListVisibility, setOrderListVisibility] = useState(false);
  const [orderId, setOrderId] = useState(0);

  useEffect(() => {
    createAPIEndpoint(ENDPOINTS.CUSTOMER)
      .fetchAll()
      .then((res) => {
        let customerList = res.data.map((item) => ({
          id: item.customerId,
          title: item.customerName,
        }));
        customerList = [{ id: 0, title: "Select" }].concat(customerList);
        setCustomerList(customerList);
      })
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    let Total = values.orderDetails.reduce((tempTotal, item) => {
      return tempTotal + item.quantity * item.foodItemPrice;
    }, 0);
    setValues({
      ...values,
      Total: roundTo2DecimalPoint(Total),
    });
  }, [values.orderDetails, setValues, values]);

  const validateForm = () => {
    let temp = {};
    temp.customerId = values.customerId !== 0 ? "" : "This field is required.";
    temp.paymentMethod =
      values.paymentMethod !== "none" ? "" : "This field is required.";
    temp.orderDetails =
      values.orderDetails.length !== 0 ? "" : "This field is required.";
    setErrors({ ...temp });
    return Object.values(temp).every((x) => x === "");
  };

  const submitOrder = (e) => {
    e.preventDefault();
    if (validateForm()) {
      createAPIEndpoint(ENDPOINTS.ORDER)
        .create(values)
        .then((res) => {
          console.log(res);
          resetFormControls();
        })
        .catch((err) => console.log(err));
    }
  };

  const resetForm = () => {
    resetFormControls();
    setOrderId(0);
  };

  const listOfOrders = () => {
    setOrderListVisibility(true);
  };

  return (
    <>
      <Form onSubmit={submitOrder}>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <Input
              disabled
              label="Order number"
              name="orderNumber"
              value={values.orderNumber}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <AdornmentText>#</AdornmentText>
                  </InputAdornment>
                ),
              }}
            />
            <Select
              sx={{ width: "100%", marginTop: 2 }}
              label="Customer"
              name="customerId"
              value={values.customerId}
              onChange={handleInputChange}
              options={customerList}
              error={errors.customerId}
            />
          </Grid>

          <Grid item xs={6}>
            <Select
              sx={{ width: "100%", marginBottom: 2 }}
              label="Payment method"
              name="paymentMethod"
              value={values.paymentMethod}
              onChange={handleInputChange}
              options={pMethods}
              error={errors.paymentMethod}
            />
            <Input
              disabled
              label="Grand total"
              name="Total"
              value={values.Total}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <AdornmentText>$</AdornmentText>
                  </InputAdornment>
                ),
              }}
            />
            <SubmitButtonGroup>
              <Button
                size="large"
                endIcon={<RestaurantMenuIcon />}
                className="resetButton"
                type="submit"
              >
                Submit
              </Button>
              <Button
                size="large"
                onClick={resetForm}
                startIcon={<ReplayIcon />}
                className="resetButton"
              >
                Reset
              </Button>
              <Button
                size="large"
                onClick={listOfOrders}
                startIcon={<ReorderIcon />}
                className="orderButton"
              >
                Orders
              </Button>
            </SubmitButtonGroup>
          </Grid>
        </Grid>
      </Form>
      <Popup
        title="List of Orders"
        openPopup={orderListVisibility}
        setOpenPopup={setOrderListVisibility}
      >
        <OrderList {...{ setOrderId, setOrderListVisibility, resetFormControls }} />
      </Popup>
    </>
  );
}
