import React from "react";
import OrderForm from "./OrderForm";
import SearchFoodItems from "./SearchFoodItems";
import OrderedFoodItems from "./OrderedFoodItems";
import { useForm } from "../../hooks/useForm";
import { Grid } from "@mui/material";

const generateOrderNumber = () =>
  Math.floor(100000 + Math.random() * 900000).toString();

const getFreshModelObject = () => ({
  invoiceId: 0,
  orderNumber: generateOrderNumber(),
  customerId: 0,
  paymentMethod: "none",
  Total: 0,
  deletedOrderItemIds: "",
  orderDetails: [],
});

export default function Order() {
  const { 
    values, 
    setValues, 
    errors, 
    setErrors,
    handleInputChange,
    resetFormControls 
  } = useForm(getFreshModelObject);

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <OrderForm
          values={values}
          setValues={setValues}
          errors={errors}
          setErrors={setErrors}
          handleInputChange={handleInputChange}
          resetFormControls={resetFormControls}
        />
      </Grid>
      <Grid item xs={6}>
        <SearchFoodItems values={values} setValues={setValues} />
      </Grid>
      <Grid item xs={6}>
        <OrderedFoodItems values={values} setValues={setValues} />
      </Grid>
    </Grid>
  );
}
