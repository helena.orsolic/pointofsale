import React from 'react';
import { Dialog, DialogTitle, DialogContent, Typography, IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

const styles = {
    dialogWrapper: {
        padding: '8px', // Adjust the padding as needed
        position: 'absolute',
        top: '40px', // Adjust the top position as needed
    },
    dialogTitle: {
        paddingRight: '0px',
    },
};

const Popup = (props) => {
    const { title, children, openPopup, setOpenPopup } = props;

    return (
        <Dialog open={openPopup} maxWidth="md" classes={{ paper: styles.dialogWrapper }}>
            <DialogTitle style={styles.dialogTitle}>
                <div style={{ display: 'flex' }}>
                    <Typography variant="h6" component="div" style={{ flexGrow: 1 }}>
                        {title}
                    </Typography>
                    <IconButton onClick={() => setOpenPopup(false)}>
                        <CloseIcon />
                    </IconButton>
                </div>
            </DialogTitle>
            <DialogContent dividers>{children}</DialogContent>
        </Dialog>
    );
};

export default Popup;
