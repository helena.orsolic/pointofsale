import React from 'react';
import { styled } from '@mui/material/styles';
import FormControl from '@mui/material/FormControl';

const FormRoot = styled(FormControl)({
  '& .MuiFormControl-root': {
    width: '90%',
    margin: 1,
  },
});

export default function Form(props) {
  const { children, ...other } = props;

  return (
    <form component={FormRoot} noValidate autoComplete="off" {...other}>
      {children}
    </form>
  );
}