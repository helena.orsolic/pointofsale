import React from "react";
import { styled } from "@mui/system";
import { Table as MuiTable } from "@mui/material";

const TableRoot = styled(MuiTable)({
  "& tbody td": {
    fontWeight: "300",
  },
  "& tbody tr:hover": {
    backgroundColor: "#fffbf2",
    cursor: "pointer",
  },
  "& .MuiTableCell-root": {
    border: "none",
  },
});

export default function Table(props) {
  return <TableRoot>{props.children}</TableRoot>;
}
