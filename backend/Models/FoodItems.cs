﻿using System.ComponentModel.DataAnnotations;

namespace backend.Models
{
    public class FoodItems
    {
        [Key]
        public int FoodItemId { get; set; }
        public string FoodItemName { get; set; }
        public decimal Price { get; set; }
    }
}
