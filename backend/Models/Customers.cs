﻿using System.ComponentModel.DataAnnotations;

namespace backend.Models
{
    public class Customers
    {
        [Key]
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
    }
}

