﻿using System.ComponentModel.DataAnnotations.Schema;

namespace backend.Models
{
    public class Invoice
    {
        public long InvoiceId { get; set; }
        public string OrderNumber { get; set; }

        public int CustomerId { get; set; }
        public Customers Customer { get; set; }

        public string PaymentMethod { get; set; }
        public decimal Total { get; set; }

        public List<OrderDetails> OrderDetails { get; set; } = new List<OrderDetails>();

        
        [NotMapped]
        public string DeletedOrderItemIds { get; set; }

    }
}
