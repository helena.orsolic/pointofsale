﻿using System.ComponentModel.DataAnnotations;

namespace backend.Models
{
    public class OrderDetails
    {
        [Key]
        public long OrderDetailId { get; set; }
        public long InvoiceId { get; set; }

        public int FoodItemId { get; set; }
        public FoodItems FoodItem { get; set; }

        public decimal FoodItemPrice { get; set; }
        public int Quantity { get; set; }
    }
}
