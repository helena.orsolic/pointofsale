﻿using backend.Models;
using Microsoft.EntityFrameworkCore;

namespace backend.Data
{
    public class ApplicationDbContext:DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options):base(options) 
        { 
        
        }

        public DbSet<Customers> Customers { get; set; }
        public DbSet<FoodItems> FoodItems { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<OrderDetails> OrderDetails { get; set; }
    }
}
