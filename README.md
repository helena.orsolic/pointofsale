# Restaurant Point of Sale App

## Overview

This is a simple Point of Sale (POS) app designed for restaurants. It allows users to create and manage orders, customers, and invoices. The app is built using React for the front end and ASP.NET Core for the back end.

## Features

- **Order Management:** Create, submit, and view restaurant orders.
- **Customer Management:** Manage customer information and associate orders with specific customers.
- **Invoice Generation:** Automatically generate invoices for completed orders.
- **Payment Methods:** Specify payment methods for orders, such as cash or card.

## Technologies Used

- **Front End:** React, Material-UI
- **Back End:** ASP.NET Core
- **Database:** Entity Framework Core
- **API Requests:** Axios
- **Styling:** CSS, Material-UI styling
- **State Management:** React Hooks

## Screenshots

![App Screenshot 1](https://gitlab.com/helena.orsolic/pointofsale/-/raw/main/Screenshots/Screenshot%20(1).jpg?ref_type=heads)

![App Screenshot 2](https://gitlab.com/helena.orsolic/pointofsale/-/raw/main/Screenshots/Screenshot%20(2).png?ref_type=heads)

![App Screenshot 3](https://gitlab.com/helena.orsolic/pointofsale/-/raw/main/Screenshots/Screenshot%20(3).png?ref_type=heads)
